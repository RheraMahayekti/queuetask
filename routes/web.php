<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', 'HomeController@index')->name('home');
Auth::routes();
Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/antrian/{id}', 'HomeController@queue');
Route::view('/perusahaan/add', 'form_perusahaan');
Route::post('/perusahaan/store', 'QueueController@store')->name('perusahaan.add');
Route::get('/perusahaan/edit/{id}', 'QueueController@edit');
Route::get('/perusahaan/delete/{id}', 'QueueController@delete');

Route::get('/perusahaan', 'QueueController@view')->name('perusahaan');
Route::get('/antrian/ambil/{id}/{chanel}/{jadwal}', 'AntrianController@tambah_antrian');
Route::get('/antrian/panggil/{id}/{chanel}', 'AntrianController@panggil_antrian');
Route::get('/antrian/stop/{id}/{chanel}', 'AntrianController@stop_antrian');

Route::get('/test', 'QueueController@test_queue');
Route::get('test-email/{id}', 'AntrianController@email');

