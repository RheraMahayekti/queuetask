<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NewMigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('user', function(Blueprint $table){
        //     $table->increments('id');
        //     $table->string('username');
        //     $table->string('email');
        //     $table->timestamps();
        // });
        Schema::create('perusahaan', function(Blueprint $table){
            $table->increments('id');
            $table->string('nama_perusahaan');
            $table->timestamps();
        });
        Schema::create('jadwal', function(Blueprint $table){
            $table->increments('id');
            $table->string('hari');
            $table->string('waktu_awal');
            $table->string('waktu_selesai');
            $table->integer('kuota');
            $table->timestamps();
        });
        Schema::create('chanel', function(Blueprint $table){
            $table->increments('id');
            $table->integer('perusahaan_id')->unsigned();
            $table->foreign('perusahaan_id')->references('id')->on('perusahaan')->onDelete('cascade');
            $table->string('nama_chanel');
            $table->string('status')->default('Aktif');
            $table->timestamps();
        });
        Schema::create('chanel_jadwal', function(Blueprint $table){
            $table->increments('id');
            $table->integer('chanel_id')->unsigned();
            $table->foreign('chanel_id')->references('id')->on('chanel')->onDelete('cascade');
            $table->integer('jadwal_id')->unsigned();
            $table->foreign('jadwal_id')->references('id')->on('jadwal')->onDelete('cascade');
            $table->timestamps();
         });
        Schema::create('antrian', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('perusahaan_id')->unsigned();
            $table->foreign('perusahaan_id')->references('id')->on('perusahaan')->onDelete('cascade');
            $table->integer('chanel_id')->unsigned();
            $table->foreign('chanel_id')->references('id')->on('chanel')->onDelete('cascade');
            $table->string('no_antrian');
            $table->string('status')->default('Menunggu');
            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
        Schema::dropIfExists('perusahaanklinik');
        Schema::dropIfExists('jadwal');
        Schema::dropIfExists('antrian');
    }
}
