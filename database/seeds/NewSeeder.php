<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\role;
class NewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        DB::table('role')->insert([
            [ 'name' => 'Admin' ],
            [ 'name' => 'User' ]
        ]);
        DB::table('permission')->insert([
            [ 'name' => 'view_data' ], //1
            [ 'name' => 'create_data' ], //2
            [ 'name' => 'edit_data' ], //3
            [ 'name' => 'delete_data' ], //4
            [ 'name' => 'view_queue' ], //5
            [ 'name' => 'call_queue' ], //6
            [ 'name' => 'stop_queue' ], //7
            [ 'name' => 'add_queue' ], //8
        ]);
        $admin = role::where('name', 'Admin')->first();
            $admin->permission()->attach([1, 2, 3, 4, 5, 6, 7]);
        $user = role::where('name', 'User')->first();
            $user->permission()->attach([5, 8]);
        
        for($i=0; $i<100; $i++)
        {
            DB::table('users')->insert([
                'name'   => $faker->firstName,
                'email' => $faker->email,
                'password' => $faker->password,
                'role_id' => '2'
            ]);
        }

        DB::table('perusahaan')->insert([
            [ 'nama_perusahaan' => 'Bank Satu' ],
            [ 'nama_perusahaan' => 'Bank Dua' ]
        ]);
        for($i=0; $i<5; $i++)
        {
            $day = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat'];
            $waktu_awal = ['08:00', '09:00', '10:00', '11:00'];
            $waktu_selesai = ['09:00', '10:00', '11:00', '12:00'];
            for($j=0; $j<4; $j++)
            {
                DB::table('jadwal')->insert([
                    'hari'          => $day[$i],
                    'waktu_awal'    => $waktu_awal[$j],
                    'waktu_selesai' => $waktu_selesai[$j],
                    'kuota'         => 100,
                ]);
            }
        }
        
        for($j=0; $j<2; $j++)
        {
            Db::table('chanel')->insert([
            [
                'perusahaan_id' => $j+1,
                'nama_chanel'   => 'Teller 1',
            ],[
                'perusahaan_id' => $j+1,
                'nama_chanel'   => 'Teller 2',
            ],[
                'perusahaan_id' => $j+1,
                'nama_chanel'   => 'Teller 3',
            ],[
                'perusahaan_id' => $j+1,
                'nama_chanel'   => 'CS 1',
            ],[
                'perusahaan_id' => $j+1,
                'nama_chanel'   => 'CS 2',
            ]]);
        }
        for($i=0; $i<10; $i++)
        {
            $random = $faker->numberBetween(1,5);
            for($j=0; $j<$random; $j++)
            {
                Db::table('chanel_jadwal')->insert([
                    'chanel_id' => $i+1,
                    'jadwal_id' => $faker->numberBetween(1,20),
                ]); 
            }
        }
        for($j=0; $j<2; $j++)
        {
            for($x=0; $x<5; $x++)
            {
                $random = $faker->numberBetween(1,5);
                DB::table('antrian')->insert([
                    'perusahaan_id'     => $j+1,
                    'chanel_id'         => $x+1,
                    'status'            => 'Diproses',
                    'no_antrian'        => 0,
                ]);
                for($i=0; $i<$random; $i++)
                {
                    DB::table('antrian')->insert([
                        'user_id'           => $faker->numberBetween(1,100),
                        'perusahaan_id'     => $j+1,
                        'chanel_id'         => $x+1,
                        'no_antrian'        => $i+1,
                    ]);
                }
            }
        }
    }
}
