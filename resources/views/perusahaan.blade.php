@extends('layouts.app')
@section('content')

<script src = "http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer ></script>
<div id="headerwrap">
  <div id="sf">
  </div>
</div> 
<a class="btn btn-success float-right" href="/perusahaan/add" id="newData"> Create New Data</a>
<div class="module-body table">
    <table id="tbl_list" class="display" width="75%">
        <thead>
            <tr>
                <th> No </th>
                <th> Nama Perusahaan</th>
                <th> Nama Chanel</th>
                <th>  </th>
            </tr>
        </thead>
        <tbody>
            @foreach($perusahaan as $parent)
            <tr>
                <td>{{$parent->id}}</td>
                <td>{{$parent->nama_perusahaan}}</td>
                <td>@foreach ($parent->chanel as $child)
                    <li>{{$child->nama_chanel}}</li>
                    @endforeach</td>
                <td>
                    <a class="btn btn-success" href="/perusahaan/edit/{{$parent->id}}" id="editData"> Edit</a>
                    <a class="btn btn-danger" href="/perusahaan/delete/{{$parent->id}}" id="deleteData"> Remove</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
