@extends('layouts.app')
@section('content')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

<div id="headerwrap">
  <div id="sf">
  </div>
</div>
<div class="container">
  <div class="table-responsive">
    <form class="form" action="{{ route('perusahaan.add') }}" id="form_perusahaan" method="POST">
      {{csrf_field()}}
      <span id="result"></span>
      <?php 
        $id = isset($edit)?$edit->id:'0';
        $tambah_id = 1; 
        $hari = null;
        $waktu = null;
      ?>
      <input type="hidden" value="{{$id}}" name="update" id="update">
      <div class="col-lg-8">
        <br>
        <label for="nama_perusahaan">Nama Perusahaan</label>
        <input type="text" name="nama_perusahaan" class="form-control" value="{{isset($edit)?$edit->nama_perusahaan:''}}" id="nama_perusahaan" placeholder="Nama Perusahaan">
      </div>
      <div class="col-lg-12">
      <br>
        <table class="table table-bordered table-striped" id="chanel_table">
          <thead>
            <tr>
              <th width="75%">Nama Chanel</th>
              <th width="25%"> </th>
            </tr>
          </thead>
          <tbody>
          @if($id != 0)
            @foreach($chanel as $data)
            <tr>
              <td><input type="text" name="nama_chanel[]" value="{{$data->nama_chanel}}" class="form-control" /></td>
                  <input type="hidden" name="id[]" value="{{$data->id}}" >
              <td>@foreach($data->jadwal as $datajadwal)
                  @if( $hari==null || $hari != $datajadwal->hari)
                    <?php $hari=$datajadwal->hari; ?>
                    <input type="checkbox" name="jadwal[]" value="{{$datajadwal->hari}}" {{ ($datajadwal->hari ? ' checked' : '') }}/> {{$datajadwal->hari}} ( {{$datajadwal->waktu_awal}} - {{$datajadwal->waktu_selesai}} ) <br>
                  @endif
                  @endforeach</td>
              
              <td><button type="button" name="remove" id="" class="btn btn-large btn-danger remove">Remove</button></td>
            </tr>
            <?php $hari=null; $waktu=null; $tambah_id++; ?>
            @endforeach
          @endif
          </tbody>
        </table>
        <button type="submit" name="submit" class="btn btn-large btn-success float-right">Submit</button>
      </div>
    </form>
  </div>
</div>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script>
  $(document).ready(function(){
    var id = "<?php $id; ?>";
    if(id == 0){
      var count=1;
      form_perusahaan(count);
      function form_perusahaan(number){
        html = '<tr>';
        html += '<td><input type="text" name="nama_chanel[]" class="form-control" /></td>';

        if(number>1){
          html += '<td><button type="button" name="remove" id="" class="btn btn-large btn-danger remove">Remove</button></td></tr>';
          $('tbody').append(html);
        }
        else{   
          html += '<td><button type="button" name="add" id="add" class="btn btn-large btn-success">Add</button></td></tr>';
          $('tbody').append(html);
        }

      }
    }
    else{
      var count = "<?php $tambah_id; ?>";
    }
    
    $(document).on('click', '.remove', function(){
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          count--;
      $(this).closest("tr").remove();
        }
      });
      
    });

    $('#add').on('click', function(){
      count++;
      form_perusahaan(count);
      $('#form_perusahaan').submit();
    });

    $('#submit').click(function(e){
      e.preventDefault();
      $.ajax({
        url : "{{ route('perusahaan.add') }}",
        method : 'post',
        data : $(this).serialize(),
        beforeSend:function(){
          $('#submit').attr('disabled','disabled');
        },
        
        success:function(data){
          if(data.error){
            var error_html = '';
            for(var count = 0; count < data.error.length; count++){
              error_html += '<p>'+data.error[count]+'</p>';
            }
            $('#result').html('<div class="alert alert-danger">'+error_html+'</div>');
          }
          else{
            form_perusahaan(1);
            $('#result').html('<div class="alert alert-success">'+data.success+'</div>');
          }
          $('#submit').attr('disabled', false);
        }
      });
      
    });
  });
</script>
@endsection