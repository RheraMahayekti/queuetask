<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Link - Bootstrap Agency Template</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="/img/favicon.png" rel="icon">
  <link href="/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic|Raleway:400,300,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link type="text/css" href="/css/app.css" rel="stylesheet">
    <link type="text/css" href="/css/apps.css" rel="stylesheet">
    <link type="text/css" href="/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="/css/theme.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="/css/style.css" rel="stylesheet">

  <!-- JavaScript Libraries -->
  <script src="/lib/jquery/jquery.min.js"></script>
  <script src="/lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="/lib/php-mail-form/validate.js"></script>
  <script src="/lib/easing/easing.min.js"></script>
  <script src="/lib/animonscroll/animonscroll.js"></script>
  <script src="/lib/modernizr/modernizr.custom.js"></script>
  <script src="/lib/masonry/masonry.min.js"></script>
  <script src="/lib/classie/classie.js"></script>
  <script src="/lib/imagesloaded/imagesloaded.js"></script>

  <!-- Template Main Javascript File -->
  <script src="/js/main.js"></script>
</head>

<body>
    @yield('sidebar')
    @yield('message')
    @yield('content')
</body>
</html>