@section('sidebar')
<!-- Menu -->
<nav class="menu" id="theMenu">
    <div class="menu-wrap">
        <h1 class="logo"><a href="/home">HOME</a></h1>
        <i class="fa fa-arrow-right menu-close"></i>
        @if(Auth::guest())
        <a href="{{ route('login') }}">{{ ('LOGIN') }}</a>
        <a href="{{ route('register') }}">{{ ('REGISTER') }}</a>
        @else
        <a href="{{ route('logout') }}" onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            {{ __('LOGOUT') }}</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        @endif
        <a href="/perusahaan">PERUSAHAAN</a>
    </div>

    <!-- Menu button -->
    <div id="menuToggle"><i class="fa fa-bars"></i></div>
  </nav>    
@endsection