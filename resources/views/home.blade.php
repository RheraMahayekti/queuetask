@extends('layouts.app')
@extends('layouts.sidebar')
@extends('layouts.message')
@section('content')

<!-- MAIN IMAGE SECTION -->
<div id="headerwrap">
    <div id="sf">
        @foreach ($perusahaan as $item)
        <div class="container">
            <div class="row">
                <div class="col-lg-12 dg">
                    <br>
                    <table class="display">
                        <thead>
                            <tr>
                                <th width=2000> <h3>{{$item->nama_perusahaan}}</h3> </th>
                                <th width=1000> <a class="btn btn-large btn-success float-center" href="/antrian/{{$item->id}}" id="lihatAntrian"> Lihat Antrian</a></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div> 
        </div> 
        <br>
        @endforeach
    </div>
</div>

<div id="copyrights">
    <div class="container">
        <p> &copy; Copyrights <strong>Link</strong>. All Rights Reserved </p>
        <div class="credits">
            Created with Link template by <a href="https://templatemag.com/">TemplateMag</a>
        </div>
    </div>
</div>
@endsection