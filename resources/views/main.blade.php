@extends('layouts.app')
@extends('layouts.sidebar')
@extends('layouts.message')
@section('content')
<!-- MAIN IMAGE SECTION -->
<div id="headerwrap">
  <div id="sf">
  @foreach ($lihat as $antri)
  @foreach ($antri->chanel->jadwal as $cekjadwal)
  @if($cekjadwal->hari == $today)
    <div class="container">
      <div class="row">
        <div class="col-lg-8 lg">
          <h4>{{$antri->chanel->nama_chanel}}</h4>
          <h4>{{$cekjadwal->hari}} :: {{$cekjadwal->waktu_awal}} - {{$cekjadwal->waktu_selesai}}</h4>
          <h2>
            {!! !empty($antri->no_antrian) ? $antri->no_antrian : '00' !!}</h2>
          <h4>> Menunggu : {!! !empty($hitung[$antri->chanel_id]) ? $hitung[$antri->chanel_id] : '0' !!}</h4>
        </div>
        <div class="sidebar">
          <div class="col-lg-4 lg">
            <br>
            <a class="btn btn-large btn-success float-center" href="/antrian/ambil/{{$antri->perusahaan_id}}/{{$antri->chanel_id}}/{{$antri->jadwal_id}}" id="ambilAntrian"> Ambil Antrian</a><br><br>
            <a class="btn btn-large btn-success float-center" href="/antrian/panggil/{{$antri->perusahaan_id}}/{{$antri->chanel_id}}" id="panggilAntrian"> Panggil Antrian</a>
            <a class="btn btn-large btn-success float-center" href="/antrian/stop/{{$antri->perusahaan_id}}/{{$antri->chanel_id}}" id="stopAntrian"> Stop Antrian</a>
          </div>
        </div>
      </div> <br>
    </div>
  @endif
  @endforeach
  @endforeach
  </div>
</div>

<div id="copyrights">
  <div class="container">
    <p> &copy; Copyrights <strong>Link</strong>. All Rights Reserved </p>
    <div class="credits">
      Created with Link template by <a href="https://templatemag.com/">TemplateMag</a>
    </div>
  </div>
</div>
@endsection