<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\antrian;
use App\perusahaan;
use App\chanel;

class jadwal extends Model
{
    protected $table = "jadwal";
    protected $fillable = ['hari', 'waktu_awal', 'waktu_selesai', 'kuota'];
    public function antrian(){
        return $this->belongsTo('App\antrian','jadwal_id');
    }
    public function jadwal(){
        return $this->belongsTo('App\perusahaan','perusahaan_id');
    }
    public function chanel(){
        // return $this->belongsToMany('App\chanel', 'chanel_jadwal', 'jadwal_id', 'chanel_id');
        return $this->belongsToMany('App\chanel');
    }
}
