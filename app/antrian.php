<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\perusahaan;
use App\jadwal;
use App\chanel;
use App\User;

class antrian extends Model
{
    protected $table = "antrian";
    protected $fillable = ['user_id', 'perusahaan_id', 'chanel_id', 'jadwal_id', 'no_antrian'];
    public function perusahaan(){
        return $this->belongsTo('App\perusahaan','perusahaan_id');
    }
    public function jadwal(){
        return $this->hasMany('App\jadwal','id');
    }
    public function chanel(){
        return $this->belongsTo('App\chanel','chanel_id');
    }
    public function user(){
        return $this->belongsTo('App\user','user_id');
    }
}
