<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class permissionrole extends Model
{
    protected $table = "permission_role";
    protected $fillable = ['role_d', 'permission_id'];
}
