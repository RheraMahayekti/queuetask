<?php

namespace App;
use App\jadwal;
use App\chanel;
use Illuminate\Database\Eloquent\Model;

class chanel_jadwal extends Model
{
    protected $table = "chanel_jadwal";
    protected $fillable = ['chanel_id', 'jadwal_id'];
    public function chanel_jadwal(){
        return $this->belongsToMany('App\chanel_jadwal', 'jadwal_id');
    }
}
