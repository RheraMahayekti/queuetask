<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    protected $table = "users";
    protected $fillable = ['name', 'email'];
    public function antrian(){
        return $this->hasMany('App\antrian','user_id');
    }
}
