<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\antrian;
use App\jadwal;
use App\chanel;
use App\jobs;
use App\user;
use App\perusahaan;
use App\chanel_jadwal;
use App\Mail\Email;
use Mail;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Queue;
use App\Jobs\QueueJob;
use Log;
use Validator;

class QueueController extends Controller
{
    public function view(){
        $this->authorize('view_data');
        $perusahaan = perusahaan::get();
        $chanel = chanel::get();
        $jadwal = jadwal::get();
        return view ('perusahaan', compact('perusahaan', 'chanel', 'jadwal'));
    }

    public function store(Request $request){
        $this->authorize('edit_data');
        if($request){
            $chanel = array(
                'nama_chanel' => 'required',
                'nama_perusahaan' => 'required'
                // 'hari' => 'required',
                // 'waktu' => 'required',
            );
            $error = Validator::make($request->all(), $chanel);
            if($error->fails()){
                return response()->json([
                    'error' => $error->errors()->all()
                ]);
            }
            if($request['update'] !=0){
                $perusahaan = perusahaan::where('id', $request->update)->first();
                $chanel = $perusahaan->chanel()->delete();
                $perusahaan->update(['nama_perusahaan' => $request->nama_perusahaan]);
                $hitung = chanel::where('perusahaan_id', $request->update)->count();
                // foreach ($request->nama_chanel as $key => $value) {
                //     $chanel = chanel::find($request->id[$key]);
                //     $chanel->nama_chanel = $request->nama_chanel[$key];
                //     $chanel->save();
                // }
                // if(count($request->id) > $hitung){
                foreach ($request->nama_chanel as $key => $value) {
                    if(!is_null($request->nama_chanel[$key])){
                        $chanel              = new chanel();
                        $chanel->perusahaan_id = $request->update;
                        $chanel->nama_chanel = $request->nama_chanel[$key];
                        $chanel->save();
                    }
                    
                }
                // }
            }
            else{
                $perusahaan = perusahaan::create(['nama_perusahaan' => $request->nama_perusahaan]);
                $perusahaan_id = $perusahaan->id;
                foreach ($request->nama_chanel as $nama_chanel) {
                    $chanel              = new chanel();
                    $chanel->perusahaan_id = $perusahaan->id;
                    $chanel->nama_chanel = $nama_chanel;
                    $chanel->save();
                }
            }
            return redirect ('/perusahaan');
        }
    }

    public function edit($id){
        $this->authorize('edit_data');
        $edit = perusahaan::find($id);
        $chanel = chanel::where('perusahaan_id', $id)->get();
        $jadwal = jadwal::get();
        $chanel_jadwal = chanel_jadwal::get();
        // dd($chanel_jadwal);
        $hitung = $chanel->count();
        return view('form_perusahaan')->with(compact('edit', 'chanel', 'hitung', 'jadwal', 'chanel_jadwal'));
    }

    public function delete($id){
        $this->authorize('delete_data');
        $delete = perusahaan::find($id)->delete();
        return redirect ('/perusahaan');
    }
}