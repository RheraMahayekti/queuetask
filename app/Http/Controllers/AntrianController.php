<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\antrian;
use App\perusahaan;
use App\chanel;
use App\jadwal;
use App\User;
use App\Mail\Email;
use Mail;
use Auth;


class AntrianController extends Controller
{
    public function tambah_antrian($perusahaan_id, $chanel_id, $jadwal_id){
        $this->authorize('add_queue');
        if(Auth::check())
        {
            $antrian = antrian::where('perusahaan_id', $perusahaan_id)->first();
            $chanel = chanel::where('id', $chanel_id)->first();
            $user = Auth::user();
            $jadwal = jadwal::where('id', $jadwal_id)->first();
            $last = antrian::select('no_antrian')
                ->where('perusahaan_id', '=', $perusahaan_id)
                ->where('chanel_id', '=', $chanel_id)
                ->count();
            $last+=1;
            if($last < ($jadwal->kuota)){
                $tambah = antrian::create([
                    'user_id' => $user->id,
                    'perusahaan_id' => $perusahaan_id,
                    'chanel_id' => $chanel_id,
                    'jadwal_id' => $jadwal_id,
                    'no_antrian' => $last,
                    'status'    => 'Menunggu',
                ]);
                $sisakuota = min($jadwal->kuota, $last);
                jadwal::create([
                    'perusahaan_id' => $perusahaan_id,
                    'hari'          => $jadwal->hari,
                    'waktu'         => $jadwal->waktu,
                    'kuota'         => $sisakuota,
                ]);
                $data = [
                    'name' => $user->name,
                    'chanel' => $chanel->nama_chanel,
                    'no_antrian' => $last,
                ];
                $mail = $user->email;
                Mail::send('mail.notifemail', ["data1"=>$data], function($message) use($mail){
                $message->to($mail)
                    ->from('mailfor.you@mail.com')
                    ->subject('Confirm Mail');
                });
                return redirect()->back()->with('success', 'Anda Berhasil Masuk Antrian! Silahkan Cek Email Anda');
            }
            return redirect()->back()->with('error', 'Antrian Sudah Penuh!!'); 
        }
        else{
            return redirect('/')->with(['error' => 'Anda Harus Login!!']);
        }
    }

    public function panggil_antrian($perusahaan_id, $chanel_id){
        $this->authorize('call_queue');
        $selesai = antrian::select()
            ->where('perusahaan_id', '=', $perusahaan_id)
            ->where('chanel_id', '=', $chanel_id)
            ->where('status', 'Diproses')
            ->first();
        $selesai->status = 'Selesai';
        $selesai->save();
        $panggil = antrian::select()
            ->where('perusahaan_id', '=', $perusahaan_id)
            ->where('chanel_id', '=', $chanel_id)
            ->where('status', 'Menunggu')
            ->first();
        $panggil->status = 'Diproses';
        $panggil->save();
        $nomor = $panggil->no_antrian;
        $user = $panggil->user_id;
        $antrian = antrian::where('no_antrian', $nomor+1)->first();
        $user2 = user::where('id', $antrian->user_id)->first();
        $data = [
            'name' => $user2->name,
            'no_antrian1' => $nomor,
            'no_antrian2' => $antrian->no_antrian,
        ];
        $mail = $user2->email;
        Mail::send('mail.reminderemail', ["data1"=>$data], function($message) use($mail){
        $message->to($mail)
            ->from('mailfor.you@mail.com')
            ->subject('Reminder Mail');
        });
        return redirect()->back()->with('success', 'Panggilan pada antrian : '.$nomor);
    }

    public function stop_antrian($perusahaan_id, $chanel_id){
        $this->authorize('stop_queue');
        $perusahaan = perusahaan::where('id', $perusahaan_id)->get();
        $chanel = chanel::select()
                    ->where('perusahaan_id', $perusahaan_id)
                    ->where('status', 'Aktif')
                    ->first();
        $antrian = antrian::select()
                    ->where('perusahaan_id', $perusahaan_id)
                    ->where('chanel_id', $chanel_id)
                    ->where('status', 'Menunggu');
        $hitung = $antrian->count();
        for($i=0; $i<$hitung; $i++){
            $antrian->first();
            $antrian->chanel_id = $chanel->id;
            $antrian->save();
        }
        
            //  dd($chanel->id);
                    
        

    }
}
