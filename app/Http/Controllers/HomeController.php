<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\antrian;
use App\jadwal;
use App\chanel;
use App\jobs;
use App\user;
use App\perusahaan;
use Carbon\Carbon;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->authorize('view_queue');
        $perusahaan = perusahaan::all();
        return view('home', compact('perusahaan'));
    }
    public function queue($id){
        $this->authorize('view_queue');
        // $today = Carbon::now()->format('l');
        $dt = new Carbon(date('Y-m-d'));
        setlocale(LC_ALL, 'id_ID', 'id_ID.utf8', 'IND', 'Indonesia', 'Indonesian');
        $today = $dt->formatLocalized('%A');
        $today ='Jumat';
        // $jadwalhari
        // dd($today);
        // $jadwalhari = jadwal::where('hari', $today)
        // ->first();
        // $jadwalchanelsekarang = Chanel
        $chanel = chanel::where('perusahaan_id', $id)
        ->join('chanel_jadwal','chanel.id','=','chanel_jadwal.chanel_id')
        ->join('jadwal','jadwal.id','=','chanel_jadwal.jadwal_id')
        ->where('jadwal.hari','=',$today)
        ->get();
        // dd($chanel);
        $total = $chanel->count();
        // $chanel->get();
        // dd($chanel);
        for($j=0; $j<$total; $j++)
        {
            $antrian = antrian::select()
                ->where('status', '=', 'Diproses')
                ->where('perusahaan_id', '=', $id)
                ->where('chanel_id', '=', $j+1)
                ->get();
            if($antrian->isEmpty())
            {
                antrian::select()
                    ->where('status', '=', 'Menunggu')
                    ->where('perusahaan_id', '=', $id)
                    ->where('chanel_id', '=', $j+1)
                    ->limit(1)->update([
                        'status' => 'Diproses',
                    ]);
            }
            $hitung[$j+1] = antrian::select()
            ->where('status', '=', 'Menunggu')
            ->where('perusahaan_id', '=', $id)
            ->where('chanel_id', '=', $j+1)
            ->count();
            $hitung[$j+1] +=1;
        }
        $lihat = antrian::select()
            ->where('status', '=', 'Diproses')
            ->where('perusahaan_id', $id)
            ->orderBy('chanel_id', 'ASC')
            ->get();
            // dd($lihat);
        return view('main', compact('lihat', 'hitung', 'chanel', 'today'));
    }
}
