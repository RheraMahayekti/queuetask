<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\perusahaan;
use App\antrian;
use App\jadwal;

class chanel extends Model
{
    protected $table = "chanel";
    protected $fillable = ['perusahaan_id', 'nama_chanel', 'jadwal_id'];
    
    public function perusahaan(){
        return $this->belongTo('App\perusahaan','perusahaan_id');
    }
    public function antrian(){
        return $this->hasMany('App\antrian','chanel_id');
    }
    public function jadwal(){
        // return $this->belongsToMany('App\jadwal', 'chanel_jadwal', 'chanel_id',  'jadwal_id');
        return $this->belongsToMany('App\jadwal');
    }
    
}
