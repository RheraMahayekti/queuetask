<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use App\chanel;
// use App\jadwal;
// use App\antrian;

class perusahaan extends Model
{
    protected $table = "perusahaan";
    protected $fillable = ['id', 'nama_perusahaan'];
    
    public function chanel(){
        return $this->hasMany('App\chanel','perusahaan_id');
    }
    public function jadwal(){
        return $this->hasMany('App\jadwal','perusahaan_id');
    }
    public function antrian(){
        return $this->hasMany('App\antrian','perusahaan_id');
    }
}
