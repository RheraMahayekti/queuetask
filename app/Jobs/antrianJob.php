<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class antrianJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($perusahaan, $chanel, $jadwal, $antrian)
    {
        $this->perusahaan_id = $perusahaan;
        $this->chanel_id = $chanel;
        $this->jadwal_id = $jadwal;
        $this->no_antrian = $antrian;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        antrian::create([
            'perusahaan_id' => $perusahaan,
            'jadwal_id' => $jadwal,
            'chanel_id' => $chanel,
            'no_antrian' => $antrian,
            'status' => 'Menunggu',
        ]);
    }
}
